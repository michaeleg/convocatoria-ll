﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SinFuegos
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void extintoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionExtinguidores fge = new FrmGestionExtinguidores();
            fge.MdiParent = this;
            fge.DsExtinguidores = dsExtinguidor;
            fge.Show();
        }

        private void extinguidoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReporteExtinguidor rp = new frmReporteExtinguidor();
            rp.DsExtinguidores = dsExtinguidor;
            rp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

        }
    }
}
