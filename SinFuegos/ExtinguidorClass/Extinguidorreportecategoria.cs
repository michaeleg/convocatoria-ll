﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinFuegos.Entities
{
    class Extinguidor
    {
        private int id;
        private Categoria categoria;
        private string tipoExtinguidor;
        private Marca marca;
        private Capacidad capacidad;
        private UnidadMedida unidadMedida;
        private string lugarDesignado;
        private DateTime fechaRecarga;
        

        public int Id
        {
            get
            {
                return Id1;
            }

            set
            {
                Id1 = value;
            }
        }

        internal Categoria Categoria
        {
            get
            {
                return Categoria1;
            }

            set
            {
                Categoria1 = value;
            }
        }

        public string TipoExtinguidor
        {
            get
            {
                return TipoExtinguidor1;
            }

            set
            {
                TipoExtinguidor1 = value;
            }
        }

        internal Marca Marca
        {
            get
            {
                return Marca1;
            }

            set
            {
                Marca1 = value;
            }
        }

        internal Capacidad Capacidad
        {
            get
            {
                return Capacidad1;
            }

            set
            {
                Capacidad1 = value;
            }
        }

        internal UnidadMedida UnidadMedida
        {
            get
            {
                return UnidadMedida1;
            }

            set
            {
                UnidadMedida1 = value;
            }
        }

        public string LugarDesignado
        {
            get
            {
                return LugarDesignado1;
            }

            set
            {
                LugarDesignado1 = value;
            }
        }

        public DateTime FechaRecarga
        {
            get
            {
                return FechaRecarga1;
            }

            set
            {
                FechaRecarga1 = value;
            }
        }

        internal UnidadMedida UnidadMedida1
        {
            get
            {
                return UnidadMedida2;
            }

            set
            {
                UnidadMedida2 = value;
            }
        }

        public int Id1
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        internal Categoria Categoria1
        {
            get
            {
                return categoria;
            }

            set
            {
                categoria = value;
            }
        }

        public string TipoExtinguidor1
        {
            get
            {
                return tipoExtinguidor;
            }

            set
            {
                tipoExtinguidor = value;
            }
        }

        internal Marca Marca1
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        internal Capacidad Capacidad1
        {
            get
            {
                return capacidad;
            }

            set
            {
                capacidad = value;
            }
        }

        internal UnidadMedida UnidadMedida2
        {
            get
            {
                return unidadMedida;
            }

            set
            {
                unidadMedida = value;
            }
        }

        public string LugarDesignado1
        {
            get
            {
                return lugarDesignado;
            }

            set
            {
                lugarDesignado = value;
            }
        }

        public DateTime FechaRecarga1
        {
            get
            {
                return fechaRecarga;
            }

            set
            {
                fechaRecarga = value;
            }
        }

        public Extinguidor(int id, Categoria categoria, string tipoExtinguidor, Marca marca, Capacidad capacidad, UnidadMedida unidadMedida, string lugarDesignado, DateTime fechaRecarga)
        {
            this.Id = id;
            this.Categoria = categoria;
            this.TipoExtinguidor = tipoExtinguidor;
            this.Marca = marca;
            this.Capacidad = capacidad;
            this.UnidadMedida = unidadMedida;
            this.LugarDesignado = lugarDesignado;
            this.FechaRecarga = fechaRecarga;
           
        }
    }

    enum Categoria
    {
        Americano,
        Europeo
    }

    enum Marca
    {
        Tornado,
        Amerex,
        Otros
    }

    enum Capacidad
    {
        Cinco,
        Diez,
        Veinte,
        Cincuenta
    }

    enum UnidadMedida
    {
        Litros,
        Libras
    }
}
