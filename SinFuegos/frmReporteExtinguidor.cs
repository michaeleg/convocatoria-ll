﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SinFuegos
{
    public partial class frmReporteExtinguidor : Form
    {
        private DataSet dsExtinguidores;
        private BindingSource bsExtinguidores;

        public DataSet DsExtinguidores
        {
            get
            {
                return dsExtinguidores;
            }

            set
            {
                dsExtinguidores = value;
            }
        }

        public BindingSource BsExtinguidores
        {
            get
            {
                return bsExtinguidores;
            }

            set
            {
                bsExtinguidores = value;
            }
        }

        public frmReporteExtinguidor()
        {
            InitializeComponent();
            bsExtinguidores = new BindingSource();
        }

        private void btnVer_Click(object sender, EventArgs e)
        {
            FrmReporte fr = new FrmReporte();
            fr.DsSistema = dsExtinguidores;
            fr.Show();

        }

        private void frmReporteExtinguidor_Load(object sender, EventArgs e)
        {
            cmbFilter.DisplayMember = "Nombres";
            cmbFilter.ValueMember = "Id";
            cmbFilter.DataSource = dsExtinguidores.Tables["Cliente"];

            bsExtinguidores.DataMember = dsExtinguidores.Tables["Extinguidor"].TableName;
            bsExtinguidores.DataSource = dsExtinguidores;

            dgvReporte.DataSource = bsExtinguidores;
            dgvReporte.AutoGenerateColumns = true;

        }

        private void cmbFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            bsExtinguidores.Filter = string.Format("Extinguidor = {0}", cmbFilter.SelectedValue);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmExtinguidor fc = new FrmExtinguidor();
            fc.DsSistema = dsExtinguidores;
            fc.TblExtinguidores = dsExtinguidores.Tables["Extinguidor"];
            fc.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvReporte.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar este registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                dsExtinguidores.Tables["Extinguidor"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
